//***************************************************************************************************				//****************************************************************************************************				
//****************************************************************************************************

//				|																	|				
//				| 				Test de prueba Angularjs - karma-jasmine  			|				
//				| 			Con validacion http   Actualizado --> 11/03/2020 		|			    

//****************************************************************************************************
//**************************************************************************************************** 
//**************************************************************************************************** 

//Comentar el código.

(function ()
{
	'use strict';

	angular.module('form_login',[]) 
	.directive('tntUserSignedup', ['$http', function ($http) 
	{
	  return { //Importante no establecer el corchete debajo, 

			    require: 'ngModel',
			    link: function(scope, elm, attrs, ctrl) 
			    {
			   
				    var urlAPI = '/users/loginForm';
				 
				 
				    ctrl.$asyncValidators.tntusersignedup = function(modelValue, viewValue) 
				    {
				    	return $http({method: 'GET', url: urlAPI, params: {alias: viewValue}});
				    };
			   
			    }
	  		};

	}]);

})();