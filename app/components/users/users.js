//****************************************************************************************************																
//**************************************************************************************************** 								
//****************************************************************************************************

//				|																	|
//				| 				Test de prueba Angularjs - karma-jasmine   			|
//				|				Con View :o		Actualizado --> 11/03/2020			|

//****************************************************************************************************
//**************************************************************************************************** 
//**************************************************************************************************** 

//---> entender el codigo y explicarlo.

(function () {

	'use strict';

	//Definimos el modulo components users y el controllador de usuarios 

	angular.module('components.users', [])
	.controller('Users_Controller',function (factory_users) 
	{
		var vm = this;

		//Call all() and set it yo users
		vm.users = factory_users.all();
	})

	.config(function($stateProvider) 
	{
		$stateProvider

		.state('users',
		{
			url: '/users',

			templateUrl: 'components/users/users.html',

			controller: 'Users_Controller as uc'
		});

	});

})();