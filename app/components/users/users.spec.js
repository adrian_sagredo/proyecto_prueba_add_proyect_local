//****************************************************************************************************																
//**************************************************************************************************** 								
//****************************************************************************************************

//				|																	|
//				| 					Test de prueba Angularjs - karma-jasmine  		|
//				|				Con View :o		Actualizado --> 11/03/2020			|

//****************************************************************************************************
//**************************************************************************************************** 
//**************************************************************************************************** 


//------------------------ Conjunto de casos de uso, controller users ------------------------
//--------------------------------------------------------------------------------------------	

describe('Users_Controllers',function () 
{
	
	var $controller,  //--> Para acdeder a todos los controladores.
		
		$compile,     //--> Un mock para poder compilar una directiva html como una elemento de js. 	
		
		$rootScope,   //--> Para acceder al scope raiz, y así acceder a los modelos de Javascript, en los archivos html.
		
		$httpBackend,  // Un mock para simular peticiones http.

		users_controller,

		factory_users; 			// Objs: objetos para realizar los test de los contralodares.


	var user_list = // Obj: lista de usuarios hardcodeada, para realizar los test.
	[
		{id: '1',name: 'Marta',role: 'Designer',location: 'new york',twitter: '@martita25'},

		{id: '2',name: 'Geriberto',role: 'Developer',location: 'France',twitter: '@theernest67'},

		{id: '3',name: 'Eustaquio',role: 'Painter',location: 'Canada',twitter: '@thepainter_sketch56'},

		{id: '4',name: 'Anastasio',role: 'Policer',location: 'Spain',twitter: '@fantasia_de_nieve78'}
	];


	//************************* Carga de modulos e init Obj test ***********************

	//Antes de los test cargamos los modulos de los controladores de los usuarios y los de la view.

	beforeEach(angular.mock.module('ui.router'));

	beforeEach(angular.mock.module('components.users'));

	beforeEach(angular.mock.module('Module_users'));

	beforeEach(angular.mock.module('form_login'));

	//Realizamos la injección de para aceder a todos los controladores.

	beforeEach(inject(function (_$controller_, _factory_users_, _$compile_, _$rootScope_,_$httpBackend_) 
	{
		$controller = _$controller_; 
		
		factory_users = _factory_users_;

		$compile = _$compile_;           //Establecemos los servicios.

		$rootScope = _$rootScope_; 

		$httpBackend = _$httpBackend_;
															//---> De todos los controladores, accedemos al de usuarios.												
		spyOn(factory_users,'all').and.callFake(function ()
		{
			return user_list;
		});												
	
	
		//Añadimos la factoria de usurios al controlador.
		users_controller = $controller('Users_Controller', {Users: factory_users});


		$httpBackend.when('GET','/users/loginForm?alias=estefania')   // hardcodeamos un Get rest, usuario no registrado
		.respond(400, {mensaje: 'usuario no registrado'});



		$httpBackend.when('GET','/users/loginForm?alias=adrian')   // hardcodeamos un Get rest, usuario registrado
		.respond(200, {mensaje: 'usuario registrado'});


		$rootScope.user = {alias: null};



	//Compilamos a formulario htlm a como un elemento angular js

	var element = angular.element('<form name="form_user"><input name="alias" type="text" ng-model="user.alias" tnt-user-signedup /></form>');


	$compile(element)($rootScope);       //Compilamos el elemento y lo establecemos en el scope raiz.
    form_user = $rootScope.form_user;    //Además podemos acceder al formulario con una variable en js.

	}));

	//+++++++++++++++++ Caso de uso existe? ctrl users ++++++++++++++++

	it('Should be defined controller - users',function () 
	{
		expect(users_controller).toBeDefined();
	}); 

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//+++++++++++++++++ Caso de uso existe? ctrl users ++++++++++++++++	

	it('Should inicialize with a call to Users.all()',function() 
	{
		expect(factory_users.all).toHaveBeenCalled(); 

		expect(users_controller.users).toEqual(user_list);
	});

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	it('should detect that alias is not register', function () {
        form_user.alias.$setViewValue('estefania');
        $rootScope.$digest();
        $httpBackend.flush();
        expect(form_user.alias.$error.tntusersignedup).toBeTruthy(); // lo del error
  });
  
  it('should detect that alias is register', function () {
        form_user.alias.$setViewValue('adrian');
        $rootScope.$digest();
        $httpBackend.flush();
        expect(form_user.alias.$valid).toBeTruthy();
  });

});

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------



