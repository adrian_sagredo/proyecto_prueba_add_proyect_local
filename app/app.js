(function() {
  'use strict';

  angular.module('meetIrl', [
    
    'ui.router',
    
    'components.users',
    
    'Module_users',

    'form_login'
  ])
  .config(function($urlRouterProvider) {
    $urlRouterProvider.otherwise("/users");
  });
})();
