//****************************************************************************************************																
//**************************************************************************************************** 								
//****************************************************************************************************

//					|																	|
//					| 				Test de prueba Angularjs - karma-jasmine  			|
//					|						Actualizado --> 11/03/2020					|

//****************************************************************************************************
//**************************************************************************************************** 
//**************************************************************************************************** 



//Se utiliza para establecer los casos de uso
describe('Users factory', function() 
{

//--------------------------  Casos de uso --------------------------

	var factory_users; //Variable en js, que representará a los usuarios 

	var user_list =
	[
		{
			id: '1',
			name: 'Marta',
			role: 'Designer',
			location: 'new york',
			twitter: '@martita25'
		},

		{
			id: '2',
			name: 'Geriberto',
			role: 'Developer',
			location: 'France',
			twitter: '@theernest67'
		},

		{
			id: '3',
			name: 'Eustaquio',
			role: 'Painter',
			location: 'Canada',
			twitter: '@thepainter_sketch56'
		},

		{
			id: '4',
			name: 'Anastasio',
			role: 'Policer',
			location: 'Spain',
			twitter: '@fantasia_de_nieve78'
		}
	];

	var single_user = 
		{
			id: '4',
			name: 'Anastasio',
			role: 'Policer',
			location: 'Spain',
			twitter: '@fantasia_de_nieve78'
		};	
	

	//Antes de cada test caragamos el modulo api users.
	beforeEach(angular.mock.module('Module_users'));

	//Antes de cada test hacemos que una funcion (Caso concreto una factoria de usuarios) la establzcamos como una variable de js, para testearla.
	beforeEach(inject(function(_factory_users_) 
	{
		
	factory_users = _factory_users_; 
	
	}));

	// Un simple test o caso de uso para verificar que la factoría Users existe

	it('Should exist factory',function() 
	{

	//******* Caso de uso del existencia users *******

	expect(factory_users).toBeDefined();

	//***********************************************

	});

  	 
	describe('.all()', function() 
	{
	//--------------Casos de uso con all --------------

		it('shoud exist function users all()', function() 
		{
		//**** Caso uso existe users.all() ****

			expect(factory_users.all).toBeDefined();

		//*************************************
		});

		it('shoud return a hard-coded list of users', function() 
		{
		//****** Caso uso lista usuarios ******

			expect(factory_users.all()).toEqual(user_list);  //---> Comprobamos si el método 

		//*************************************
		});

	//-----------------------------------------------
	});

	describe('.find_by_id()',function() 
	{
	  //+++++++++++++++++++++++++++++++++++++++++++++++++++++

		//--- Casos de uso exist users.find_by_id() ----

		it('should exist users function find_by_id()',function() {
			
			expect(factory_users.find_by_id).toBeDefined(); //--> está definido el método.
		});

		//------------------------------------------

		//----- Casos de uso busqueda usuario ------
						
		it('should return a user by id',function() {
			
			expect(factory_users.find_by_id('4')).toEqual(single_user); //--> está definido el método.
		});
						
		//------------------------------------------


		//-- Casos de uso de busqueda usuario no encontrado --
						
		it('should return undefined if user not found ',function() {
			
			expect(factory_users.find_by_id('abc')).not.toBeDefined(); //--> está definido el método.
		});
						
		//--------------------------------------------

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++

	});
	   



	/*
	//Se utiliza para establecer el test en cuestión.
	it('has a dummy spec to test 2 + 2',function() {
		
   //******* Caso de uso usuarios *******		

		expect(2).toEqual(4);

   //************************************
	});

	*/

//------------------------------------------------------------------
});

