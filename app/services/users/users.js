//****************************************************************************************************																
//**************************************************************************************************** 								
//****************************************************************************************************

//					|																	|
//					| 				Test de prueba Angularjs - karma-jasmine   			|
//					|						Actualizado --> 11/03/2020					|

//****************************************************************************************************
//**************************************************************************************************** 
//**************************************************************************************************** 

(function() 
{
	'use strict';


	//Creamos el modulo y la factoria que hemos referenciado en el bloque beforeEach en nuestro archivo de test.

	angular.module('Module_users', [])
	.factory('factory_users', function() 
	{
		var factoria_users = {};

		var user_list =
	[
		{
			id: '1',
			name: 'Marta',
			role: 'Designer',
			location: 'new york',
			twitter: '@martita25'
		},

		{
			id: '2',
			name: 'Geriberto',
			role: 'Developer',
			location: 'France',             //---> Nos generamos una lista.
			twitter: '@theernest67'
		},

		{
			id: '3',
			name: 'Eustaquio',
			role: 'Painter',
			location: 'Canada',
			twitter: '@thepainter_sketch56'
		},

		{
			id: '4',
			name: 'Anastasio',
			role: 'Policer',
			location: 'Spain',
			twitter: '@fantasia_de_nieve78'
		}
	];

		//users.method = function() {};

		 factoria_users.all = function() //--> método all.
		 {
		 	return user_list;   //y retornamos la lista.
    	 };

		factoria_users.find_by_id = function(id) 
		{
			//return user_list[id];

				
	      return user_list.find(function(user) 
			      {
			        		return user.id === id;
			      });
		};
		
		return factoria_users;
	});
})();